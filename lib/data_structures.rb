# EASY

# Write a method that returns the range of its argument (an array of integers).
def range(arr)
  arr.max - arr.min
end

# Write a method that returns a boolean indicating whether an array is in sorted
# order. Use the equality operator (==), which returns a boolean indicating
# whether its operands are equal, e.g., 2 == 2 => true, ["cat", "dog"] ==
# ["dog", "cat"] => false
def in_order?(arr)
  return true if arr == arr.sort
  false
end


# MEDIUM

# Write a method that returns the number of vowels in its argument
def num_vowels(str)
  str.count('a') +
  str.count('e') +
  str.count('i') +
  str.count('o') +
  str.count('u') +
  str.count('A') +
  str.count('E') +
  str.count('I') +
  str.count('O') +
  str.count('U')
end

# Write a method that returns its argument with all its vowels removed.
def devowel(str)
  str.delete!('a')
  str.delete!('e')
  str.delete!('i')
  str.delete!('o')
  str.delete!('u')
  str.delete!('A')
  str.delete!('E')
  str.delete!('I')
  str.delete!('O')
  str.delete!('U')
  str
end


# HARD

# Write a method that returns the returns an array of the digits of a
# non-negative integer in descending order and as strings, e.g.,
# descending_digits(4291) #=> ["9", "4", "2", "1"]
def descending_digits(int)
  digits = int.to_s.split('')
  digits.sort.reverse
end

# Write a method that returns a boolean indicating whether a string has
# repeating letters. Capital letters count as repeats of lowercase ones, e.g.,
# repeating_letters?("Aa") => true
def repeating_letters?(str)
  return false if str.downcase.chars.uniq == str.downcase.chars
  true
end

# Write a method that converts an array of ten integers into a phone number in
# the format "(123) 456-7890".
def to_phone_number(arr)
  phone_number = ["("] + arr[0..2] + [") "] + arr[3..5] + ["-"] + arr[6..9]
  phone_number.join
end

# Write a method that returns the range of a string of comma-separated integers,
# e.g., str_range("4,1,8") #=> 7
def str_range(str)
  numbers = str.split(",").sort
  numbers[-1].to_i - numbers[0].to_i
end


#EXPERT

# Write a method that is functionally equivalent to the rotate(offset) method of
# arrays. offset=1 ensures that the value of offset is 1 if no argument is
# provided. HINT: use the take(num) and drop(num) methods. You won't need much
# code, but the solution is tricky!
def my_rotate(arr, offset=1)
  true_offset = offset % arr.length
  new_arr = arr + arr.take(true_offset)
  true_offset.times { new_arr.delete_at(0) }
  new_arr
end

# Finish by April 28 - EOD
